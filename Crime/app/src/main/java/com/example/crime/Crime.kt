package com.example.crime

data class Crime(
    var title:String,
    val id:Long,
    var date:Date,
    var time:Time,
    var isSolved:Boolean
)
