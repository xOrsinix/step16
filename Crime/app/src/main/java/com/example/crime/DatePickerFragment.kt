package com.example.crime

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels

class DatePickerFragment:DialogFragment() {
    private val model:CrimeModel by activityViewModels()
    lateinit var btSelect: Button
    lateinit var etDate:EditText
    lateinit var btConfirm:Button

    private var lastSelectedYear:Int = 2002
    private var lastSelectedMonth:Int = 1
    private var lastSelectedDay:Int = 26


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView:View = inflater.inflate(R.layout.date_dialog,container,false)
        btSelect = rootView.findViewById(R.id.btSelect)
        etDate = rootView.findViewById(R.id.etDate)
        btConfirm=rootView.findViewById(R.id.btConfirm)
        lastSelectedYear=model.date.value!!.year
        lastSelectedDay=model.date.value!!.day
        lastSelectedMonth=model.date.value!!.month
        etDate.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                return true
            }

        })
        etDate.setText(model.date.value.toString())
        btSelect.setOnClickListener {
            buttonSelectDate()
        }
        btConfirm.setOnClickListener {
            dismiss()
        }
        return rootView
    }

    private fun buttonSelectDate(){
        val dateSetListner = object: DatePickerDialog.OnDateSetListener{
            override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
                etDate.setText("$p3/$p2/$p1")
                lastSelectedYear=p1
                lastSelectedMonth=p2
                lastSelectedDay=p3
                model.date.value = Date(year=p1,month = p2,day =p3)
            }
        }
        val datePickerDialog = DatePickerDialog(requireContext(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
            dateSetListner,lastSelectedYear,lastSelectedMonth,lastSelectedDay)
        datePickerDialog.show()
    }
}

