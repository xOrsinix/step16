package com.example.crime

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import com.example.crime.databinding.FragmentCrimeBinding

class CrimeFragment : Fragment() {
    private val model:CrimeModel by activityViewModels()

    private val crime = Crime("Birth",0,Date(26,1,2002), Time(21,6),true)

    lateinit var binding: FragmentCrimeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCrimeBinding.inflate(inflater)
        model.date.value=crime.date
        model.time.value=crime.time
        binding.etTitle.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                crime.title = p0.toString()
            }
        })

        model.date.observe(activity as LifecycleOwner){
            crime.date = model.date.value!!
            binding.tvDateText.text=crime.date.toString()
        }

        model.time.observe(activity as LifecycleOwner){
            crime.time=model.time.value!!
            binding.tvTimeText.text=crime.time.toString()
        }

        binding.btChangeTime.setOnClickListener {
            val dialog = TimePickerFragment()
            dialog.show(requireActivity().supportFragmentManager,"customDialog")
        }

        binding.btChange.setOnClickListener {
            val dialog = DatePickerFragment()
            dialog.show(requireActivity().supportFragmentManager,"customDialog")
        }

        binding.cbSolved.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                crime.isSolved=p1
            }
        })
        init()
        return binding.root
    }



    fun init(){
        val mod = crime
        binding.tvIDtext.text=mod.id.toString()
        binding.tvDateText.text=mod.date.toString()
        binding.tvTimeText.text=mod.time.toString()
        binding.etTitle.setText(mod.title)
        binding.cbSolved.isChecked = mod.isSolved
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            CrimeFragment()
    }
}