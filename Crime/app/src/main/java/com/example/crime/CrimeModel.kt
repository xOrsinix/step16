package com.example.crime

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CrimeModel:ViewModel() {
    val date:MutableLiveData<Date> by lazy{
        MutableLiveData<Date>()
    }

    val time:MutableLiveData<Time> by lazy {
        MutableLiveData<Time>()
    }
}