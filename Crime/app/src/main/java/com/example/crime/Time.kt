package com.example.crime

class Time(
    var hours:Int,
    var minutes:Int
) {

    override fun toString(): String {
        if (minutes<10)
            return "$hours:0$minutes"
        else
            return "$hours:$minutes"
    }
}