package com.example.crime

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels

class TimePickerFragment:DialogFragment() {
    private val model:CrimeModel by activityViewModels()
    lateinit var btSelect: Button
    lateinit var etDate: EditText
    lateinit var btConfirm: Button

    private var is24H = true
    private var lastSelectedHour:Int = 2002
    private var lastSelectedMinute:Int = 1


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView: View = inflater.inflate(R.layout.date_dialog,container,false)
        btSelect = rootView.findViewById(R.id.btSelect)
        etDate = rootView.findViewById(R.id.etDate)
        btConfirm=rootView.findViewById(R.id.btConfirm)
        lastSelectedHour=model.time.value!!.hours
        lastSelectedMinute=model.time.value!!.minutes
        etDate.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                return true
            }

        })
        etDate.setText(model.time.value.toString())
        btSelect.setOnClickListener {
            buttonSelectDate()
        }
        btConfirm.setOnClickListener {
            dismiss()
        }
        return rootView
    }

    private fun buttonSelectDate(){
        val timeSetListener = object : TimePickerDialog.OnTimeSetListener{
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
                etDate.setText("${p1}:${p2}")
                lastSelectedHour=p1
                lastSelectedMinute=p2
                model.time.value = Time(p1,p2)
            }
        }
        val timePickerDialog = TimePickerDialog(requireContext(),timeSetListener,lastSelectedHour,lastSelectedMinute,is24H)
        timePickerDialog.show()
    }
}