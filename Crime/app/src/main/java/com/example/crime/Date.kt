package com.example.crime

data class Date(var day:Int,var month:Int,var year:Int){

    override fun toString(): String {
        return "$day/$month/$year"
    }

}
